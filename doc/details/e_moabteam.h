/*! \page team MOAB team members

 <h2>Current Team: </h2>
 
 - Vijay S. Mahadevan (Argonne National Lab)
 - Iulian Grindeanu (Argonne National Lab) 
 - Rajeev Jain (Argonne National Lab)
 - Paul Wilson (Univ Wisconsin-Madison)
 - Patrick Shriwise (Argonne National Lab)

 <h2>Emeritus members:</h2>
 
 - Timothy J. Tautges (Siemens, Univ Wisconsin-Madison)
 - Danqing Wu  (Argonne National Lab)
 - Navamita Ray (Los Alamos National Lab)
 - Jane Hu (Univ Wisconsin-Madison)
 - Anthony Scopatz (The University of South Carolina)
 - Jason A. Kraftcheck
 - Brandon M. Smith
 - Hong-Jun Kim
 - Jim Porter
 - Xiabing Xu
 - Milad Fatenejad
 
*/
